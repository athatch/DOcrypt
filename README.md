# DOcrypt - simplied JWT creation and string signing for Cloudflare Durable Objects

This is a durable object designed to remove the complexities from webcrypto and allow simple creation and verification of json web tokens and simple signing and verification of strings.

It's designed to cover the basic tasks of hashing strings for password storage/login verification and JWT generation/verification for authentication/session management (which should be combined with user lookup through workers KV or equivalent - ie don't store and validate users solely through the JWT)


## Installation
Clone the repository
Update the account_id (I don't think this should be in the repository but short of creating a new Cloudflare user with access to a single account I haven't figured out a way to pass this in.  It would be good if --account-id could be passed as options to wrangler dev) and issuer binding in wrangler.toml (see below)
```bash
npm install
wrangler publish
```

## How to use

Docrypt was developed to be used through binding to another worker or cloudflare pages site so no publicly available route is necessary (though be useful for testing).

Each DOCrypt object has it's own crypto key pair which is managed through the object name (and is expected to be passed as the idFromName header):
```javascript
  const version = '1'
  const id = env.DOCRYPT.idFromName(version);
  let obj = env.DOCRYPT.get(id);
  return obj.fetch(``, {
    headers: {
      issuer: env.ISSUER
    },
  });
```

Each object instantiated with a new version will first create and store a public and private key.  This makes it easy to roll keys by incrementing the id.  Older keys can still be supported by tracking the version number.  For example, if you are hashing password for a user stored in workers KV include the version as one of the attributes for the user.  Should you need to roll the key you can either force the user to reset their password or allow login then reset with the old version.

The six key functions are:
1. Public key publishing
2. Sign/hash a string
3. Verify a string
4. Generate a JWT from claims
5. Verify and return claims from a JWT
6. Remove all storage to allow for deletion of object


### Public key publishing
To return the public key in pem format use a GET request to get the path with the version number but no sub path or query string eg:
```javascript
  const version = '1'
  const id = env.DOCRYPT.idFromName(version);
  let obj = env.DOCRYPT.get(id);
  return obj.fetch(`/`, {
    headers: {
      issuer: env.ISSUER
    },
  });
```
returns a text body
````
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4qQo+S5z/hbfRjPRtUZ733gJ1eLJEhUlwBBv4KW1gWtEwULnh4y655NiWOyXv8oW1/FSz6L5QVN64NWsxADplCyT9m/Nl6xeHXsp9QKFkaUP5Paxg6uWGe3DoLudINocFCpOT8G2i8ALcUT/9kUQKurGo3gwt16Nzppw4OWHdaKTjQM35OzlSvkHNGEfkET/l10DTN+kxIh18D0SjRCvAmKRVBZa8pdfrUG3AJ0ZEHrapu3/iQBXuUpuKyXfKy3nyYoyfrB+mj2afBemQu1gIe1svSGxkTAOonc9g2oWo8Rmoix7nmimP8DXhtnOWt698OjWbD/hsdetcuRexOqJfQIDAQAB
-----END PUBLIC KEY-----
````

### Sign/hash a string
To sign a string POST the string in the request body eg:
```javascript
  const version = '1'
  const id = env.DOCRYPT.idFromName(version);
  let obj = env.DOCRYPT.get(id);
  return obj.fetch(`/`, {
    method: 'POST',
    body: 'passw0rd',
    headers: {
      issuer: env.ISSUER
    },
  });
```
returns a text body
````
Gl3Sua4l+Pg5Mwjey7rWbN5Ug38V/JIsR8BcTA/9wydn2P4ogNYd80AksYcj7zqtnwxR3rCBdkSZKaAUAyUr4+ksLjL6xBzOnPkPIgcRcvwZdy1dWNlPXsFiHRmJas8AeWUq9vlCB1sBgwOCntZq6tlA5YQ2Wfnzh52RVDE42V2KCJAo44dddfiBfRVD4yx2cUGb1DSrYQlvxd1kawUG+F6v9iQoBPMf4ZwKE2M9gH72dwFa+uSJiCrySPYWZxyne1upCvtANVWwJK2MLZJtr6ldxaHtGqz19KxxnO+HKR9EPHBQzIM8scIzoqlfANQOozsefMdaMkUSI6xdvwuUSg==
````

### Verify a string
To verify a string pass the plain text and the signed text as a query string pair in a GET request (note they may need to be uri encoded) eg:
```javascript
  const version = '1'
  const id = env.DOCRYPT.idFromName(version);
  let obj = env.DOCRYPT.get(id);
  const plain = 'passw0rd';
  const signed = encodeURIComponent(`Gl3Sua4l+Pg5Mwjey7rWbN5Ug38V/JIsR8BcTA/9wydn2P4ogNYd80AksYcj7zqtnwxR3rCBdkSZKaAUAyUr4+ksLjL6xBzOnPkPIgcRcvwZdy1dWNlPXsFiHRmJas8AeWUq9vlCB1sBgwOCntZq6tlA5YQ2Wfnzh52RVDE42V2KCJAo44dddfiBfRVD4yx2cUGb1DSrYQlvxd1kawUG+F6v9iQoBPMf4ZwKE2M9gH72dwFa+uSJiCrySPYWZxyne1upCvtANVWwJK2MLZJtr6ldxaHtGqz19KxxnO+HKR9EPHBQzIM8scIzoqlfANQOozsefMdaMkUSI6xdvwuUSg==`);
  return obj.fetch(`/?${plain}=${signed}`, {
    headers: {
      issuer: env.ISSUER
    },
  });
```
returns a 200 response with the plain text version of the string if the signature is verified
````
passw0rd
````
or returns a 400 response if the signature is invalid:
````
Invalid signature
````

### Generate a JWT from claims
To generate a json web token from a series of claims pass the claims as JSON in a POST request (you must include the content-type header).  You should include an 'exp' claim with a value such as '3d' to set an expiry date of three days.  If no exp claim is included it will default to two hours eg:
```javascript
  const version = '1'
  const id = env.DOCRYPT.idFromName(version);
  let obj = env.DOCRYPT.get(id);
  return obj.fetch('/`, {
    method: 'POST',
    body: JSON.stringify({someClaim: 'some value', exp: '12h'}),
    headers: {
      issuer: env.ISSUER
      'Content-Type': 'application/json'
    },
  });
```
returns a text body
````
eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzb21lQ2xhaW0iOiJzb21lIHZhbHVlIiwiaXNzIjoiaHR0cHM6Ly9hdGhhdGNoLmNvbSIsImV4cCI6MTY1NDAxNDI3NiwiaWF0IjoxNjUzOTcxMDc2fQ.WfxC_ZLbdZOWvqBLRQz8TuoFZjgifhnZ5gHEDHyUz1AXPaa-KpnOXTocVlBv72grbjdGUJYslpt9fqCzSupU-aLrDN6C0o77r9M54GTg1ppoftl9tsxJhpO99GkMwBXToGjyfcC_zZfUqLp__teHciLbuu4isp9seLjTbYhWfugnPxvvRdO9qNmyUDA1vTRG3g7oZ0iVDo7P75cK4bhV5p5t-b1DKF8HXZxJPiUy4BjNnbpoJ2-qtT6DF_dOE3kP7luK6MOWpzYkwRygBk24FcxBxSLp5oo58meqYDM0YffSXZMB5sDqQrv1micYuWA382ikZ8LgFdYs_KMijl875A
````

### Verify and return claims from a JWT
To verify a json web token and get the claims pass the token as the sub path in a GET request eg:
```javascript
  const version = '1'
  const id = env.DOCRYPT.idFromName(version);
  let obj = env.DOCRYPT.get(id);
  const token = `eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzb21lQ2xhaW0iOiJzb21lIHZhbHVlIiwiaXNzIjoiaHR0cHM6Ly9hdGhhdGNoLmNvbSIsImV4cCI6MTY1NDAxNDI3NiwiaWF0IjoxNjUzOTcxMDc2fQ.WfxC_ZLbdZOWvqBLRQz8TuoFZjgifhnZ5gHEDHyUz1AXPaa-KpnOXTocVlBv72grbjdGUJYslpt9fqCzSupU-aLrDN6C0o77r9M54GTg1ppoftl9tsxJhpO99GkMwBXToGjyfcC_zZfUqLp__teHciLbuu4isp9seLjTbYhWfugnPxvvRdO9qNmyUDA1vTRG3g7oZ0iVDo7P75cK4bhV5p5t-b1DKF8HXZxJPiUy4BjNnbpoJ2-qtT6DF_dOE3kP7luK6MOWpzYkwRygBk24FcxBxSLp5oo58meqYDM0YffSXZMB5sDqQrv1micYuWA382ikZ8LgFdYs_KMijl875A`;
  return obj.fetch(`/${tokeb}`, {
    headers: {
      issuer: env.ISSUER,
      idFromName: version
    },
  });
```
returns a 200 response with the claims as json if the signature was verified
````
{"someClaim":"some value","iss":"https://athatch.com","exp":1654014538,"iat":1653971338}
````
or returns a 400 response if the signature is invalid:
````
Invalid signature
````

### Remove all storage to allow for deletion of object
Make a DELETE request to clear out the object storage which should allow for it to be deleted (apparently this works but seems hard to confirm) eg:
```javascript
  const version = '1'
  const id = env.DOCRYPT.idFromName(version);
  let obj = env.DOCRYPT.get(id);
  return obj.fetch(`/`, {
    method: 'DELETE',
    headers: {
      issuer: env.ISSUER
    },
  });
```
returns a text body
````
Version 1 cleared for deletion
````

## Issuer

The plan for the future is to allow cross-site JWT verification therefore every JWT generated must include an issuer URL which points to the public key pem that can be used to verify the token.  So any service which calls a Docrypt object should be prepared to return the public key pem on some publicly available URL and that URL should be passed as the issuer header.

Both JWT generation and verification require the issuer header.  When verifying the JWT the claims are checked for the issuer.  If it is different to the one passed then the public key will be retrieved from the URL.

An issuer is not required for calls other than JWT generation and verification however it's probably good practice to include it as per the examples above.


## Testing

Recommended option for testing Durable Object seems to be miniflare/jest as per: https://github.com/cloudflare/miniflare/blob/master/docs/src/content/testing/jest.md
Under this set up you need to have type: "module" in package.json which then means with the jose dependency you have to build to test or you get this error
````
VMScriptRunnerError [ERR_MODULE_RULE]: Unable to resolve "src/index.mjs" dependency "jose": no matching module rules.
    If you're trying to import an npm package, you'll need to bundle your Worker first.
````
Testing local may be faster but still not exactly the same set up as deployment as the build process might be different.  I could remove the jose dependecy (it's a straight forward task to create and verify jwt's but that isn't really a sustainable option longer with other more complex developments)

With wrangler dev you have effectively access to an interface with which to test requests.  The issue with wrangler dev is it doesn't detach to a separate process by default.  We can get around this by using the screen command.  Hence tests a run via the ./test.sh shell script which launches wrangler dev, runs the tests, then quits wrangler dev.  Currently it's running on the port 18787 so this should avoid conflicts of wrangler dev is already running on a default port.

The test waits for wrangler start up by polling localhost on that port until a response is received.  Every second run this fails for reasons I haven't figured out yet (maybe rate limiting by Cloudflare???).  Simply rerunning tests again seems to sort the problem out.