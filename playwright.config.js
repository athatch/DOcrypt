// playwright.config.js
// @ts-check
/** @type {import('@playwright/test').PlaywrightTestConfig} */
const config = {
  use: {
    // All requests we send go to this API endpoint.
    baseURL: 'http://localhost:18787',
    extraHTTPHeaders: {
      'issuer': 'https://test.athatch.com',
      'Access-Key': 'thisismysecret'
    },
  }
};
module.exports = config;