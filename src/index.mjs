import * as jose from "jose";

export default {
  async fetch(request, env) {
    return await handleRequest(request, env);
  },
};

async function handleRequest(request, env) {
  if (request.headers.get('Access-Key') !== env.ACCESS_KEY) {
    return new Response('Forbidden',{ status: 403 })
  }
  const version = request.headers.get('idFromName');
  if (!version) return new Response('Version must be supplied in a request header called idFromName ',{status: 400})
  const id = env.DOCRYPT.idFromName(version);
  let obj = env.DOCRYPT.get(id);
  return obj.fetch(request, {
    headers: {
      issuer: env.ISSUER,
      "Content-Type": request.headers.get("Content-Type")
    },
  });
}

export class DOcrypt {
  cryptoAlgo = {
    name: "RSASSA-PKCS1-v1_5",
    hash: "SHA-256",
  };

  ab2str(buffer) {
    return String.fromCharCode.apply(null, Array.from(new Uint8Array(buffer)));
  }

  str2ab(str) {
    const buf = new ArrayBuffer(str.length);
    const bufView = new Uint8Array(buf);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
      bufView[i] = str.charCodeAt(i);
    }
    return buf;
  }

  async exportCryptoKey(key, type) {
    const exported = await crypto.subtle.exportKey(
      type.toUpperCase() == "PRIVATE" ? "pkcs8" : "spki",
      key
    );
    const exportedAsString = this.ab2str(exported);
    const exportedAsBase64 = btoa(exportedAsString);
    const pemExported = `-----BEGIN ${type.toUpperCase()} KEY-----\n${exportedAsBase64}\n-----END ${type.toUpperCase()} KEY-----`;

    return pemExported;
  }

  async importCryptoKey(pem, type) {
    // fetch the part of the PEM string between header and footer
    const pemHeader = `-----BEGIN ${type.toUpperCase()} KEY-----`;
    const pemFooter = `-----END ${type.toUpperCase()} KEY-----`;
    const pemContents = pem.substring(
      pemHeader.length,
      pem.length - pemFooter.length
    );
    // base64 decode the string to get the binary data
    const binaryDerString = atob(pemContents);
    // convert from a binary string to an ArrayBuffer
    const binaryDer = this.str2ab(binaryDerString);

    return await crypto.subtle.importKey(
      type.toUpperCase() == "PRIVATE" ? "pkcs8" : "spki",
      binaryDer,
      this.cryptoAlgo,
      true,
      [type.toUpperCase() == "PRIVATE" ? "sign" : "verify"]
    );
  }

  async generateKeys() {
    const generatedKeyPair = await crypto.subtle.generateKey(
      {
        modulusLength: 2048,
        publicExponent: new Uint8Array([1, 0, 1]),
        ...this.cryptoAlgo,
      },
      true,
      ["sign", "verify"]
    );
    const privateKeyPem = await this.exportCryptoKey(
      generatedKeyPair.privateKey,
      "private"
    );
    const publicKeyPem = await this.exportCryptoKey(
      generatedKeyPair.publicKey,
      "public"
    );
    return {
      privateKey: generatedKeyPair.privateKey,
      publicKey: generatedKeyPair.publicKey,
      privatePem: privateKeyPem,
      publicPem: publicKeyPem,
    };
  }

  constructor(state, env) {
    this.state = state;
    this.keys = {};
    console.log('this id',this.state.id.toString())

    this.state.blockConcurrencyWhile(async () => {
      this.keys.publicPem = await this.state.storage.get("publicKey");
      if (!this.keys.publicPem) {
        this.keys = await this.generateKeys();
        await Promise.all([
          this.state.storage.put("publicKey", this.keys.publicPem),
          this.state.storage.put("privateKey", this.keys.privatePem),
        ]);
      }
    });
  }

  async publicKey() {
    if (!this.keys.publicKey) {
      this.keys.publicKey = await this.importCryptoKey(
        this.keys.publicPem,
        "public"
      );
    }
    return this.keys.publicKey;
  }

  async privatePem() {
    this.keys.privatePem =
      this.keys.privatePem || (await this.state.storage.get("privateKey"));
    return this.keys.privatePem;
  }

  async privateKey() {
    if (!this.keys.privateKey) {
      const pem = await this.privatePem();
      this.keys.privateKey = await this.importCryptoKey(pem, "private");
    }
    return this.keys.privateKey;
  }

  async sign(plainText) {
    const key = await this.privateKey();
    const signed = await crypto.subtle.sign(
      this.cryptoAlgo,
      key, //from generateKey or importKey above
      this.str2ab(plainText) //ArrayBuffer of data you want to sign
    );
    return btoa(this.ab2str(signed));
  }

  async verifySignature(plainText, signedText) {
    const key = await this.publicKey();
    let result = await crypto.subtle.verify(
      this.cryptoAlgo,
      key,
      this.str2ab(atob(signedText)),
      this.str2ab(plainText)
    );
    return result;
  }

  async tokenize(claims, exp) {
    const key = await this.privateKey();
    const token = await new jose.SignJWT(claims)
      .setProtectedHeader({
        typ: "JWT",
        alg: "RS256",
      })
      .setIssuer(this.issuer)
      .setExpirationTime(exp)
      .setIssuedAt()
      .sign(key);
    return token;
  }

  getIssuer(jwt) {
    const parts = jwt.split(".");
    const claimsB64 = parts[1];
    const claimsStr = atob(claimsB64);
    const claims = JSON.parse(claimsStr);
    return claims.iss;
  }

  async verifyToken(jwt) {
    // get key for issuer which may be us
    const issuer = this.getIssuer(jwt);
    let key;
    if (issuer !== this.issuer) {
      const keyPem = await this.fetch(issuer);
      key = importCryptoKey(keyPem, "public");
    } else {
      key = await this.publicKey();
    }
    try {
      const { payload, protectedHeader } = await jose.jwtVerify(jwt, key);
      return payload;
    } catch (e) {
      if (e.message === "signature verification failed") {
        return false;
      } else {
        throw e;
      }
    }
  }

  async fetch(request) {
    this.issuer = request.headers.get("issuer");
    const url = new URL(request.url);
    const parts = url.pathname.split("/");
    const path = parts[1]; 
    if (request.method === "GET") {
      //GET request with no path or query always returns the public key
      //Otherwise path is assumed to be a jwt token
      if (!path) {
        const params = Array.from(url.searchParams);
        if (params.length > 0) {
          const [plain, sig] = params[0];
          const ok = await this.verifySignature(plain, sig);
          return new Response(ok ? plain : "Invalid signature", {
            status: ok ? 200 : 400,
          });
        } else {
          return new Response(this.keys.publicPem);
        }
      } else {
        const payload = await this.verifyToken(path);
        if (payload) {
          return new Response(JSON.stringify(payload), {
            headers: {
              "content-type": "application/json;charset=UTF-8",
            },
          });
        } else {
          return new Response("Invalid signature", { status: 400 });
        }
      }
    }
    // post json body is the claims we want to tokenize (note should have an exp claim with expiry eg: exp: '6h')
    // or text body is the string we want to sign
    if (request.method === "POST") {
      const ct = request.headers.get("Content-Type");
      if (ct && ct.split(";")[0] === "application/json") {
        const data = await request.json();
        let { exp, ...claims } = data;
        if (!exp) exp = '2h';
        const token = await this.tokenize(claims, exp);
        return new Response(token);
      } else {
        const plain = await request.text();
        const signed = await this.sign(plain);
        return new Response(signed);
      }
    }

    if (request.method === 'DELETE') {
      await this.state.storage.deleteAll(); //empty storage so this object gets cleaned up
      return new Response(`DOcrypt object ready for deletion`)
    }

    return new Response("Method not allowed", { status: 405 });
  }
}
