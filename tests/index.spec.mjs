import { test, expect } from "@playwright/test";

let sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

let publicPem;

test.beforeAll(async ({ request }) => {
  // Create a new repository
  let started;
  let count = 0
  do {
    await sleep(2000);
    try {
      count += 1;
      const response = await request.get(`/`,{headers: {idFromName: '1'}});
      publicPem = await response.text();
      started = true;
    } catch (err) {
      started = false;
      if (count > 10) {
        throw (err)
      }
    }
  } while (!started);
  expect(started).toBeTruthy();
});

test("Request without a version fails", async ({ request }) => {
  const response = await request.get(`/`);
  expect(response.ok()).toEqual(false);
});


test("Get request with no path returns public key", async ({ request }) => {
  expect(publicPem.startsWith('-----BEGIN PUBLIC KEY-----')).toEqual(true);
});

test("Get request to same object returns the same public key", async ({ request }) => {
  const response = await request.get(`/`,{headers: {idFromName: '1'}});
  const pem = await response.text();
  expect(pem).toEqual(publicPem);
});

test("Get request to different object returns different public key", async ({ request }) => {
  const response = await request.get(`/`,{headers: {idFromName: '2'}});
  const pem = await response.text();
  expect(pem).not.toEqual(publicPem);
});

let plainText = 'passw0rd';
let signedText;

test("Post with text body returns signed text", async ({ request }) => {
  const response = await request.post(`/`, { data: plainText, headers: { idfromName: '1', 'Content-Type': 'text/plain' } });
  signedText = await response.text();
  expect(response.ok()).toEqual(true);
  expect(signedText).toBeTruthy();
});

test("Get comparing plain text to signed text verifies", async ({ request }) => {
  const response = await request.get(`/?${plainText}=${encodeURIComponent(signedText)}`,{headers: {idFromName: '1'}});
  expect(response.ok()).toEqual(true);
  const text = await response.text();
  expect(text).toEqual(plainText);
});

test("Get comparing different text to signed text fails", async ({ request }) => {
  const response = await request.get(`/?not${plainText}=${encodeURIComponent(signedText)}`,{headers: {idFromName: '1'}});
  expect(response.ok()).toEqual(false);
});

test("Get comparing plain text to invalid signature fails", async ({ request }) => {
  const response = await request.get(`/?${plainText}=${encodeURIComponent('!' + signedText)}`,{headers: {idFromName: '1'}});
  expect(response.ok()).toEqual(false);
});

let jwt
let claims = { test: 'will pass', exp: '6h' }

test("Post with json body returns json web token", async ({ request }) => {
  const response = await request.post(`/`, { data: claims, headers: { idfromName: '1', 'Content-Type': 'application/json' } });
  jwt = await response.text();
  expect(response.ok()).toEqual(true);
  expect(jwt).toBeTruthy();
  expect(jwt.split('.').length).toEqual(3);
});

test("Get with json web token verifies", async ({ request }) => {
  const response = await request.get(`/${jwt}`,{headers: {idFromName: '1'}});
  expect(response.ok()).toEqual(true);
  const data = await response.json();
  expect(data.test).toEqual(claims.test);
});

test("Get with invalid json web token false", async ({ request }) => {
  const response = await request.get(`/?${jwt}x`,{headers: {idFromName: '1'}});
  expect(response.ok()).toEqual(false);
});

test("Expired token fails to verify", async ({ request }) => {
  let response = await request.post(`/1/`, { data: {test: 'will fail', exp: '1s'}, headers: {idFromName: '1', 'Content-Type': 'application/json' } });
  const jwt = await response.text();
  expect(response.ok()).toEqual(true);
  await sleep(1100);
  response = await request.get(`/${jwt}`,{headers: {idFromName: '1'}});
  expect(response.ok()).toEqual(false);
});

test("Get with valid json web token fails with different version", async ({ request }) => {
  const response = await request.get(`/?${jwt}`,{headers: {idFromName: '2'}});
  expect(response.ok()).toEqual(false);
});

test("Delete with version returns clean up response", async ({ request }) => {
  const response = await request.delete(`/`,{headers: {idFromName: '1'}});
  expect(response.ok()).toEqual(true);
  const text = await response.text();
  expect(text).toEqual('DOcrypt object ready for deletion')
});